import React, { useState, useEffect } from "react";
//  Css
import "../App.css";

// Socket
import io from "socket.io-client";
import DataService from "../Api/requestAdminApi";
const socket = io.connect("http://192.168.1.35:7000/");

function HomePage() {
  // Form
  const [user, setUser] = useState("");
  const [mode, setMode] = useState("");
  const [map, setMap] = useState("");

  // UserData
  const [userData, setUserData] = useState("");
  const [modeData, setModeData] = useState("");
  const [mapData, setMapData] = useState("");

  const [messageData, setMessageData] = useState("");

  const [data, setData] = useState([]);

  const SubmitData = (e) => {
    e.preventDefault();
    socket.emit("addUsers", {
      user_id: user,
      mode_preference: mode,
      map_preference: map,
      room_id: socket.id,
    });
    socket.on("activeUsers", (res) => {
      setData(res);
    });
    socket.on("message", (res) => setMessageData(res));
  };
  const ClearData = (e) => {
    e.preventDefault();
    socket.emit("deleteUsers", () => {
      socket.on("activeUsers", (res) => setData(res));
    });
    socket.on("message", (res) => setMessageData(res));
  };
  const getAllData = async () => {
    const ResultUser = await DataService.GetUser();
    const ResultMode = await DataService.GetMap();
    const ResultMap = await DataService.GetMode();
    setUserData(ResultUser.data.data);
    setModeData(ResultMode.data.data);
    setMapData(ResultMap.data.data);
  };

  useEffect(() => {
    getAllData();
    socket.on("activeUsers", (res) => setData(res));
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <div className="myForms">
          <img
            src="https://hindwarfare.com/img/logo.png"
            className="App-logo"
            alt="logo"
          />
          <h1>Socket Testing Environment</h1>

          <form>
            <select name="" onChange={(e) => setUser(e.target.value)}>
              <option>Select User</option>
              {userData &&
                userData.map((item) => {
                  return (
                    <option key={item._id} value={item._id}>
                      {item.name} - {item.xp}
                    </option>
                  );
                })}
            </select>
            <select name="" onChange={(e) => setMode(e.target.value)}>
              <option>Select Mode</option>
              {modeData &&
                modeData.map((item) => (
                  <option key={item._id} value={item._id}>
                    {item.title}
                  </option>
                ))}
            </select>
            <select name="" onChange={(e) => setMap(e.target.value)}>
              <option>Select Map</option>
              {mapData &&
                mapData.map((item) => (
                  <option key={item._id} value={item._id}>
                    {item.title}
                  </option>
                ))}
            </select>

            <div className="actionbtn">
              <button onClick={SubmitData}>Test Socket</button>
              <button onClick={ClearData} className="delete">
                Delete Data
              </button>
            </div>
          </form>
        </div>
        <h1
          style={{
            color:
              messageData === "All Waiting Users Data Deleted"
                ? "#c80b0d"
                : "#14cc14",
          }}
        >
          {messageData && messageData}
        </h1>
        <div className="">
          <table className="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Sr No.</th>
                <th scope="col">User ID</th>
                <th scope="col">XP</th>
                <th scope="col">Room</th>
                <th scope="col">Mode</th>
                <th scope="col">map</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {data &&
                data.map((item, index) => {
                  return (
                    <tr key={item._id}>
                      <th scope="row">{index + 1}</th>
                      <td>{item.user_id}</td>
                      <td>{item.xp}</td>
                      <td>{item.room_id}</td>
                      <td>{item.mode_preference}</td>
                      <td>{item.map_preference}</td>
                      <td>{item.status ? "True" : "false"}</td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </header>
    </div>
  );
}

export default HomePage;
