import React, { useState } from "react";

// Socket
import io from "socket.io-client";
const socket = io.connect("http://192.168.1.35:7000/");

const RoomData = (props) => {
  const [room_id, setRoomId] = useState("");
  const SubmitData = (e) => {
    e.preventDefault();

    socket.emit("getRoomUsers", room_id);
    socket.on("roomAllUsers", (res) => console.log("res " + res));
  };
  return (
    <div className="App">
      <header className="App-header">
        <div className="myForms">
          <img
            src="https://hindwarfare.com/img/logo.png"
            className="App-logo"
            alt="logo"
          />
          <h1>Socket Testing Room</h1>

          <form>
            <input
              name="room_id"
              placeholder="Room Id"
              onChange={(e) => setRoomId(e.target.value)}
            />
            <div className="actionbtn" onClick={SubmitData}>
              <button>Submit</button>
            </div>
          </form>
        </div>
      </header>
    </div>
  );
};
export default RoomData;
