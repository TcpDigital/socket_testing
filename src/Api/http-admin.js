import axios from "axios";
export const BASEURL = {
  ENDPOINT_URL: "http://192.168.1.35:7000/api/user/",
};
export default axios.create({
  baseURL: `${BASEURL.ENDPOINT_URL}`,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8",
  },
});
