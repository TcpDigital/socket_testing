import https from "./http-admin";
class DataServiceAdmin {
  GetUser() {
    return https.get("user/all");
  }
  GetMap() {
    return https.get("map/all");
  }
  GetMode() {
    return https.get("mode/all");
  }
  SubmitBTN() {
    return https.get("mode/all");
  }
}

export default new DataServiceAdmin();
