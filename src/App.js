import React from "react";

// Core Components
import HomePage from "./Pages/HomePage";
import RoomData from "./Pages/SecondPage";

// react router dom
import { BrowserRouter, Switch, Route } from "react-router-dom";
const App = (props) => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact render={(props) => <HomePage {...props} />} />
        <Route
          path="/roomdata"
          exact
          render={(props) => <RoomData {...props} />}
        />
      </Switch>
    </BrowserRouter>
  );
};
export default App;
